# -*- coding: utf-8 -*-
"""
Created on Tue Aug 30 16:05:30 2016

@author: Nayef
"""

from pathlib import Path
import csv
import random
import argparse


def main(class_size, file_name, everything):
    p = Path('release_runway_0.2/data/images')
    S = []
    F = []
    R = []
    P = []
    
    for d_r in p.iterdir():
        for d in d_r.iterdir():
            if d.name[0] == 'S':
                for f in d.iterdir():
                    S.append(str(f))
            elif d.name[0] == 'F':
                for f in d.iterdir():
                    F.append(str(f))
            elif d.name[4:7] == 'RST':
                for f in d.iterdir():
                    R.append(str(f))
            elif d.name[4:6] == 'PF':
                for f in d.iterdir():
                    P.append(str(f))
            elif everything:
                for f in d.iterdir():
                    F.append(str(f))
    
    
    print(len(S))
    print(len(P))    
    print(len(F))    
    print(len(R))
    
    if len(S) + len(F) < class_size and everything:
        print('not enough data')
    else:
        if not everything:
            S_size = min(class_size, len(S))
            P_size = min(class_size, len(P))
            F_size = min(class_size, len(F))
            R_size = min(class_size, len(R))
            
            S = random.sample(S, S_size)
            P = random.sample(P, P_size)
            F = random.sample(F, F_size)
            R = random.sample(R, R_size)
            
            with open(file_name, 'w', newline='') as csvfile:
                csvWriter = csv.writer(csvfile)
                for s in S:
                    csvWriter.writerow([s, 0])
                for p in P:
                    csvWriter.writerow([p, 1])
                for f in F:
                    csvWriter.writerow([f, 2])
                for r in R:
                    csvWriter.writerow([r, 3])
        else:
            U = S + F
            U = random.sample(U, class_size)
            with open(file_name, 'w', newline='') as csvfile:
                csvWriter = csv.writer(csvfile)
                for u in U:
                    csvWriter.writerow([u, 0])
        
if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("FILENAME", help = "CSV file path/name")
    parser.add_argument("-cs", "--clasize", type=int, default = 100000, 
                        help = "Number of sampled picture by class. Default : 100k")
    parser.add_argument("-a", "--allrunway", action="store_true", 
                        help=("If true take a sample of all runway and put in only one class"))
    args = parser.parse_args()
    
    main(args.clasize, args.FILENAME, args.allrunway)