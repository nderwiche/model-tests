# -*- coding: utf-8 -*-
"""
Created on Fri Sep  2 12:12:06 2016

@author: Nayef
"""

import scipy.io as io
from pathlib import Path
from bisect import bisect_left
import csv
import json
import random
import argparse
import operator
import time


def binary_search(a, x, lo=0, hi=None):  
    hi = hi if hi is not None else len(a) 
    pos = bisect_left(a,x,lo,hi)          
    return (pos if pos != hi and a[pos] == x else -1)

    
def main(p1='release_runway_0.2/data/metadata.mat', 
         p2='release_runway_0.2/data/images'):

    p2 = Path(p2)
    metada = io.loadmat(p1)
    metada = metada['data'][0]    
    metada = sorted(metada, key= operator.itemgetter(0))
    metada_find = []
    for m in metada:
        metada_find.append(m[0][0])
    
    json_list=[]
    for d_r in p2.iterdir():
        for d in d_r.iterdir():
            m = metada[binary_search(metada_find, d.name)]
            if len(m)==8:
                for p in d.iterdir():
                    if len(m[4])>0:
                        timestamp = time.mktime(time.strptime(m[4][0], "%B %d, %Y")) - time.timezone
                    else:
                        timestamp = -1
                        
                    if len(m[5])>0:
                        location = m[5][0]
                    else:
                        location = "NA"
                        
                    if len(m[1])>0:
                        collection = m[1][0]
                    else:
                        collection = "NA"
                        
                    if len(m[2])>0:
                        brand = m[2][0]
                    else:
                        brand= "NA"
                        
                    if len(m[7])>0:
                        content = m[7][0]
                    else:
                        content = "NA"
                        
                    if len(m[6])>0:
                        author = m[6][0]
                    else:
                        author = "NA"
                        
                
                    json_list.append(json.dumps({'timestamp': timestamp, 'location': location, 
                                      'collection': collection, 'brand': brand, 
                                      'content': content, 'author': author,
                                      'path': str(p)}))
            else:
                print(str(d))
    
    f = open('runway.json', 'w')
    for x in json_list:
        f.write(x + '\n')
    f.close()
    
if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-p1", "--path1", default = 'release_runway_0.2/data/metadata.mat',
                        help = "path to metadata file")
    parser.add_argument("-p2", "--path2", default = 'release_runway_0.2/data/images/', 
                        help = "path to images folder")
    
    args = parser.parse_args()
    
    main(p1 = args.path1,p2 = args.path2)
    
