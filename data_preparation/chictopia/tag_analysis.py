# -*- coding: utf-8 -*-
"""
Created on Mon Aug 29 16:57:36 2016

@author: Nayef
"""

import json
import urllib
from PIL import Image
import random
import operator
import matplotlib.pyplot as plt

#tag class

#class Tag(object):
#    
#    hierarchy = {('Summer', 'summer')} #TODO : maintain property : depth = 1 and unique parent, when modifying, rehash data
#    
#    def __init__(self, tag_name):
#        self.name = tag_name
#    
#    def get_parent(self):
#        for h in Tag.hierarchy:
#            if h[0] == self.name:
#                return h[1]
#        return self.name
#        
#    def __str__(self):
#        return self.get_parent()
#    
#    def __repr__(self):
#        return self.get_parent()
#        
#    def __eq__(self, other):
#        return self.get_parent() == other.get_parent()
#        
#    def __is__(self, other):
#        return self.get_parent() == other.get_parent()
#    
#    def __hash__(self):
#        return hash(self.get_parent())
        

#data initialisation :

def open_data_source(path):
    data_source = []
    f = open(path, 'r', encoding = 'UTF-8')
    for idx,line in enumerate(f):
        data_source.append(json.loads(line))
    
    f.close()
    return data_source

def make_data_tuple(data_source):
    data = []

    for d in data_source:
        for m in d['media']:
            tag_list = [t for t in d['parse_tags']]     
            data.append((m, d['id'], frozenset(tag_list)))
    
    return data

#funcions

def open_image(d):
    file = urllib.request.urlopen(d[0])
    img = Image.open(file)
    img.show()    
    
def open_image_sample(d_iterable, k):
    ensemble = set(d_iterable)
    sample = random.sample(ensemble, k)
    for s in sample:
        open_image(s)

def count_all_tags(d_iterable):
    tags = {}    
    for d in d_iterable:
        for t in d[2]:
            if t in tags:
                tags[t] += 1
            else:
                tags[t] = 1
    sorted_tags = sorted(tags.items(), key = operator.itemgetter(1), reverse=True)
    return sorted_tags
    
def count_tags(d_iterable, tags_filter):
    tags = {}    
    for d in d_iterable:
        for t in d[2]:
            if t in tags_filter:
                if t in tags:
                    tags[t] += 1
                else:
                    tags[t] = 1
    sorted_tags = sorted(tags.items(), key = operator.itemgetter(1), reverse=True)
    return sorted_tags
    
def get_all_tags(d_iterable):
    tags = set()
    for d in d_iterable:
        for t in d[2]:
            if t not in tags:
                tags.add(t)
    return tags
    
def el_with_tag_any(d_iterable, tags):
    new_list = []
    for d in d_iterable:
        if len(d[2] & tags)>0:
            new_list.append(d)
    return new_list

def el_with_tag_all(d_iterable, tags):
    new_list = []
    for d in d_iterable:
        if len(d[2] & tags)==len(tags):
            new_list.append(d)
    return new_list

def set_inter(a, b):
    a = set(a)
    b = set(b)
    return(list(a & b))

def set_union(a, b):
    a = set(a)
    b = set(b)
    return(list(a | b))

def set_xor(a, b):
    a = set(a)
    b = set(b)
    return(list(a ^ b))
    
def tag_pair_analysis(tag1, tag2):
    ens1 = el_with_tag_any(data, {tag1})
    ens2 = el_with_tag_any(data, {tag2})
    int_ens = set_inter(ens1,ens2)
    return len(int_ens)/len(ens1), len(int_ens)/len(ens2)
    
def ens_pair_analysis(ens1, ens2):
    int_ens = set_inter(ens1,ens2)
    return len(int_ens)/len(ens1), len(int_ens)/len(ens2)
    
def matrix_tag_ens(data, tag_list):
    E = []
    L = []
    
    M = [[0 for x in range(len(tag_list))] for y in range(len(tag_list))] 
    
    i = 0.
    tot = len(tag_list)
    for t in tag_list:
        E.append((el_with_tag_any(data, {t}), t))
        L.append(t)
        print(float(i/tot))
        i += 1
    i = 0.
    t = len(E)**2
        
    for idx1, e1 in enumerate(E):
        for idx2, e2 in enumerate(E):
            if idx1 < idx2:
                a1, a2 = ens_pair_analysis(e1[0], e2[0])
                M[idx1][idx2] = (a1,a2)
                M[idx2][idx1] = (a2,a1)
                print(i*2/t)
                i += 1
    return L, M
    
    
def ens_analysis(data, tag_list, tol):
    E = []
    
    S = []
    I = []
    D = []
    
    i = 0.
    tot = len(tag_list)
    for t in tag_list:
        E.append((el_with_tag_any(data, {t}), t))
        print(float(i/tot))
        i += 1

    i = 0.
    t = len(E)**2    
    for idx1, e1 in enumerate(E):
        for idx2, e2 in enumerate(E):
            if idx1 < idx2:
                a1, a2 = ens_pair_analysis(e1[0], e2[0])
                if a1 > 1 - tol and a2 > 1 - tol:
                    S.append((e1[1], e2[1], (a1, a2)))
                if (a1 > 1 - tol and a2 < 1 - tol) or (a1 < 1 - tol and a2 > 1 - tol):
                    I.append((e1[1], e2[1], (a1, a2)))
                if a1 < tol and a2 < tol:
                    D.append((e1[1], e2[1], (a1, a2)))
                print(i*2/t)
                i += 1
    return S, I, D
    
def save_matrix_tag(path, ml):
    f = open(path, 'w')

    j = json.dumps(ml)
    f.write(j)
    
    f.close()
    
def load_matrix_tag(path):
    f = open(path, 'r')
    
    j = json.load(f)
    return j


#main
data_source = open_data_source("chictopia.json")
data = make_data_tuple(data_source)

data_source2 = open_data_source("/cached/chictopia_cached_sort.json")
data_source3 = open_data_source("/cached/chictopia_sort.json")

data_source_sorted = sorted(data_source, key = operator.itemgetter("id"))
data_source2_sorted = sorted(data_source2, key = operator.itemgetter("id"))



#script

"""
x = []
y = []
ensemble = set(data)
sample = random.sample(ensemble, 1000)
i = 0
for s in sample:
    i += 1
    file = urllib.request.urlopen(s[0])
    img = Image.open(file)
    x.append(img.width)
    y.append(img.height)
    print(i)
    
plt.hist(x)
plt.hist(y)
"""

