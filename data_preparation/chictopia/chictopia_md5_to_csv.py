# -*- coding: utf-8 -*-
"""
Created on Mon Sep  5 16:08:50 2016

@author: Nayef
"""
import json
import csv
import random
import argparse

def md5_to_path(md5):
  return "/srv/data/casher/img/"+md5[0:3]+"/"+md5[3:6]+"/"+md5[6:9]+"/"+md5[9:12]+"/"+md5

def main(size, filename, md5):
    f = open(md5, "r")
    l = json.loads(f.read())
    f.close()
    l = random.sample(l, size)
    with open(filename, 'w', newline='') as csvfile:
        csvWriter = csv.writer(csvfile)
        for x in l:
            csvWriter.writerow([md5_to_path(x['md5']), 1])
            
if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("FILENAME", help = "CSV file path/name")
    parser.add_argument("MD5", help = "MD5 list file path/name")
    parser.add_argument("-cs", "--clasize", type=int, default = 100000, 
                        help = "Number of sampled picture. Default : 100k")
    args = parser.parse_args()
    main(args.clasize, args.FILENAME, args.MD5)