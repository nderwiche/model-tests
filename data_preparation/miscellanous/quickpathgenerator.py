# -*- coding: utf-8 -*-
"""
Created on Mon Sep  5 17:58:05 2016

@author: Nayef
"""

from pathlib import Path
import csv

p = Path("source_")

L = []

for d in p.iterdir():
    L.append(str(d))
    
with open("pathfile_", 'w', newline='') as csvfile:
    csvWriter = csv.writer(csvfile)
    for l in L:
        csvWriter.writerow([l])
